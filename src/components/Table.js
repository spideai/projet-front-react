import React from "react";
import {
    DataGrid,
    GridColumnsToolbarButton,
    GridFilterToolbarButton,
    GridToolbarContainer,
    GridToolbarExport
} from '@material-ui/data-grid';
import Button from '@material-ui/core/Button';
import '../styles/Table.css';
import EditCellDialog from "./EditCellDialog";


const columns = [
    {
        field: 'id',
        headerName: 'id',
        width: 70,
        hidden: true,
        headerAlign: 'center',
        align: "center"
    },
    {
        field: 'firstname',
        headerName: 'First name',
        width: 130,
        headerAlign: 'center',
        align: "center",
    },
    {
        field: 'lastname',
        headerName: 'Last name',
        width: 130,
        headerAlign: 'center',
        align: "center"
    },
    {
        field: 'gender',
        headerName: 'Gender',
        type: 'text',
        width: 100,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'contact_phone',
        headerName: 'Phone number',
        type: 'number',
        width: 110,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'contact_email',
        headerName: 'Email',
        type: 'text',
        width: 110,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'contact_address',
        headerName: 'Adress',
        type: 'text',
        width: 110,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'contact_city',
        headerName: 'City',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true

    },
    {
        field: 'contact_location_lat',
        headerName: 'Lattitude',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true

    },
    {
        field: 'contact_location_lon',
        headerName: 'Longitude',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'preferences_favorite_color',
        headerName: 'Favorite color',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'preferences_favorite_fruit',
        headerName: 'Favorite fruit',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'preferences_favorite_movie',
        headerName: 'Favorite movie',
        type: 'text',
        width: 180,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
    {
        field: 'preferences_favorite_pet',
        headerName: 'Favorite pet',
        type: 'text',
        width: 150,
        headerAlign: 'center',
        align: "center",
        filterable: true
    },
];

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: {
                editing: false,
                editingId: -1,
                field: "",
                previousValue: "",
                value: "",
            },
            peoples: props.peoples
        };
    }

    onEditingSave = (value) => {
        const { peoples, edit } = this.state;
        peoples[edit.editingId - 1][edit.field] = value
    };

    onStartEditing = (params) => {
        this.setState({edit: {
                editing: true,
                editingId: params.row.id,
                field: params.field,
                previousValue: params.value,
                value: params.value
            }})
    };

    onEndEditing = () => {
      this.setState({
          edit: {
              editing: false,
              editingId: -1,
              field: "",
              previousValue: "",
              value: "",
          }
      })
    };

    onColumnHeaderSelected = (event) => {
        if (event.field === "__check__") {
            this.props.onRowSelected(event)
        } else {
            this.props.setSelectedColumn(event.field)
        }
    };

    CustomToolbar = () => {
        return (
            <GridToolbarContainer>
                <GridColumnsToolbarButton />
                <GridFilterToolbarButton />
                <GridToolbarExport />
                <Button onClick={this.onDownloadJSONPressed}>EXPORT JSON</Button>
            </GridToolbarContainer>
        );
    };

    onDownloadJSONPressed = () => {
        let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.state.peoples));
        let dlAnchorElem = document.createElement('a');
        dlAnchorElem.setAttribute("href",dataStr);
        dlAnchorElem.setAttribute("download", "peoples.json");
        dlAnchorElem.click();
    };


    render() {
        const { onRowSelected, peoples } = this.props;
        const { edit } = this.state;
        return (
            <div className="tableContainer">
                {
                    edit.editing && (
                        <EditCellDialog
                            open={edit.editing}
                            field={edit.field}
                            handleClose={this.onEndEditing}
                            initialValue={edit.previousValue}
                            onSave={this.onEditingSave}/>
                    )
                }
                <DataGrid
                    rowsPerPageOptions={[5, 10, 20]}
                    pagination
                    components={{
                        Toolbar: this.CustomToolbar,
                    }}
                    onCellClick={this.onStartEditing}
                    rows={peoples}
                    columns={columns}
                    pageSize={15}
                    autoHeight
                    checkboxSelection
                    onRowSelected={onRowSelected}
                    onColumnHeaderClick={this.onColumnHeaderSelected}
                />
            </div>

        );
    }
}

export default Table;
