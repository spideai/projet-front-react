import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import "../styles/EditCellDialog.css"

export default function EditCellDialog(props) {
    const { initialValue, open, handleClose, field } = props;
    const [text, setText] = React.useState(initialValue);

    const handleSave = () => {
        const { onSave } = props;
        onSave(text);
        handleClose()
    };

    return (
        <div>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Edit</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Change user {field}
                        </DialogContentText>
                        <TextField
                            autoFocus
                            placeholder={text}
                            margin="dense"
                            value={text}
                            onChange={(event) => setText(event.target.value)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="inherit">
                            <p className="buttonCancel">cancel</p>
                        </Button>
                        <Button onClick={handleSave} >
                            <p className="buttonSave">save</p>
                        </Button>
                    </DialogActions>
            </Dialog>
        </div>
    );
}