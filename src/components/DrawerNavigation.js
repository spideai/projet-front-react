import React from 'react';
import '../styles/DrawerNavigation.css';
import Drawer from '@material-ui/core/Drawer';

export default function TemporaryDrawer(props) {
    const { isOpen, setNavigationOpen, setViewId } = props;

    const onNavigationClicked = (id) => {
        setViewId(id);
        setNavigationOpen(false)
    };

    return (
        <div>
            <Drawer anchor="left"  open={isOpen} onClose={() => setNavigationOpen(false)} >
                <div className="drawerNavigationContainer">
                        <div className="NavigationItem" onClick={() => onNavigationClicked(0)}><p>Table</p></div>
                        <div className="NavigationItem" onClick={() => onNavigationClicked(1)}><p>Pie chart</p></div>
                        <div className="NavigationItem" onClick={() => onNavigationClicked(2)}><p>Histogram</p></div>
                        <div className="NavigationItem" onClick={() => onNavigationClicked(3)}><p>Map</p></div>
                </div>
            </Drawer>
        </div>
    );
}