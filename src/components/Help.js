import React from "react";
import Button from '@material-ui/core/Button';
import HelpIcon from '@material-ui/icons/Help';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from "@material-ui/core/Typography";
import DialogActions from "@material-ui/core/DialogActions";

export default function Help() {
    const [modalOpen, setModalOpen] = React.useState(false);

    const handleClose = () => {
        setModalOpen(false)
    };
    return (
        <div style={{paddingTop: "3rem"}} className="HelpContainer">
            <Button onClick={() => setModalOpen(!modalOpen)} variant="contained">
                <p style={{color: "#3C3C3C"}}>HELP</p>
                <HelpIcon style={{marginLeft: "1rem"}} />
            </Button>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={modalOpen}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Besoin d'aide ?
                </DialogTitle>
                <DialogContent dividers>
                    <Typography variant="h6" gutterBottom>
                        La barre de recherche
                    </Typography>
                    <Typography gutterBottom>
                        Quand aucune colonne n'est sélectionnée,
                        la recherche se fait sur toutes les colonnes,
                        la recherche n'est pas sensible à la casse.
                        Pour sélectionner une colonne, il suffit de cliquer sur le header de cette colonne.
                    </Typography>
                    <Typography variant="h6" gutterBottom>
                        Les graphiques
                    </Typography>
                    <Typography gutterBottom>
                        Par défaut, la colonne sélectionnée pour les graphiques est la colonne "gender",
                        pour la changer il suffit de cliquer sur un header de colonne,
                        le graphique se construira automatiquement par rapport a cette colonne.
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        <p style={{color: "#3C3C3C"}}> c'est compris</p>
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


