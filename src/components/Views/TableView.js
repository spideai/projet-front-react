import React from "react";
import Table from "../Table";
import Help from "../Help";
import '../../styles/TableView.css';

export default function TableView(props) {
    const {peoples, onRowSelected, setSelectedColumn, setPeoples} = props;
    return (
        <div className="TableViewContainer">
            <Table setPeoples={setPeoples} setSelectedColumn={setSelectedColumn} onRowSelected={onRowSelected} peoples={peoples}/>
            <Help />
        </div>
    );
}


