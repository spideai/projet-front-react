import React from 'react';
import GoogleMapReact from 'google-map-react';
import RoomIcon from '@material-ui/icons/Room';
import Tooltip from '@material-ui/core/Tooltip';


const MarkerComponent = ({name}) => (
        <Tooltip title={name}>
            <RoomIcon />
        </Tooltip>
    );

function MapView(props) {
    const {peoples} = props;

    const config = {
        center: {
            lat: 46,
            lng: 2
        },
        zoom: 0
    };

    const markers = peoples.map((person) =>
        <MarkerComponent
            key={person.contact_location_lat}
            lat={person.contact_location_lat}
            lng={person.contact_location_lon}
            name={`${person.firstname}  ${person.lastname}`}
        />
    );


    return (
        <div style={{height: '100vh', width: '100%'}}>
            <GoogleMapReact
                bootstrapURLKeys={{key: "AIzaSyAgZhDkO4mgfciiPm-v0aBEuZv9xG0I37I"}}
                defaultCenter={config.center}
                defaultZoom={config.zoom}
            >
                {
                    markers
                }
            </GoogleMapReact>
        </div>
    );
}

export default MapView;