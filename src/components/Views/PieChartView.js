import React from "react";
import ReactApexChart from 'react-apexcharts'
import "../../styles/PieChartView.css"
import serialise from "../../helpers";

export default function PieChartView(props) {
    const {peoples, selectedColumn} = props;
    const {entryNames, entryValues} = serialise(peoples, selectedColumn);
    const data = {
        series: entryValues,
        options: {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: entryNames,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        },
    };

    return (
        <div className="PieChartViewContainer">
            <ReactApexChart options={data.options} series={data.series} type="pie" width={1000} />
        </div>
    );
}
