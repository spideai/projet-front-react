import React from "react";
import ReactApexChart from 'react-apexcharts'
import "../../styles/PieChartView.css"
import serialise from "../../helpers";

export default function BarChartView(props) {
    const {peoples, selectedColumn} = props;
    const {entryNames, entryValues} = serialise(peoples, selectedColumn);
    const data = {
        options: {
            chart: {
                id: "basic-bar"
            },
            xaxis: {
                categories: entryNames
            }
        },
        series: [
            {
                name: "series-1",
                data: entryValues
            }
        ]
    };

    return (
        <div className="PieChartViewContainer">
            <ReactApexChart options={data.options} series={data.series} type="bar" width={1000} />
        </div>
    );
}
