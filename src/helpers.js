export default function serialise(peoples, selectedColumn) {
    let entryNames = [],
        entryValues = [],
        values = [],
        prev;

    peoples.forEach((person) => {
        values.push(person[selectedColumn])
    });

    values.sort();
    for (let i = 0; i < values.length; i++) {
        if (values[i] !== prev) {
            entryNames.push(values[i]);
            entryValues.push(1);
        } else {
            entryValues[entryValues.length - 1]++;
        }
        prev = values[i];
    }

    return {entryNames, entryValues}
};

