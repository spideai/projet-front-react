import React from "react";
import './App.css';
import Header from "./components/Header";
import DrawerNavigation from "./components/DrawerNavigation";
import TableView from "./components/Views/TableView";
import PieChartView from "./components/Views/PieChartView";
import BarChartView from "./components/Views/BarChatView";
import MapView from "./components/Views/MapView";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viewId: 0,
            navigationOpen: false,
            peoples: [],
            search: "",
            loading: true,
            selectedPeoples: [],
            filteredPeoples: undefined,
            selectedColumn: "",
        };
    }

    componentDidMount() {
        this.getPeoples()
    }

    setNavigationOpen = (isOpen = !this.state.navigationOpen) => {
        this.setState({navigationOpen: isOpen})
    };

    onRowSelected = (event) => {
        const { selectedPeoples, peoples } = this.state;
        if (event.field === "__check__") {
            if (selectedPeoples.length === 0) {
                this.setState({selectedPeoples: peoples})
            } else {
                this.setState({selectedPeoples: []})
            }
            return;
        }

        if (event.isSelected) {
            selectedPeoples.push(event.data);
        } else {
            const index = selectedPeoples.findIndex(people => people.id === event.data.id)
            selectedPeoples.splice(index, 1);
        }
        this.setState({selectedPeoples})
        // selectedPeoples.push()
    };

    // --- JEAN Louis way of flattening objects ---

    // code pulled from the overflow ;) i understand it
    flattenObj = (obj, parent, res = {}) => {
        for(let key in obj){
            let propName = parent ? parent + '_' + key : key;
            if(typeof obj[key] == 'object'){
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    };

    //my code
    flattenPeoples = (arr) => {
        return new Promise(resolve => {
            let newPeoples = [];
            arr.forEach((people) => {
                newPeoples.push(this.flattenObj(people))
            });
            resolve(newPeoples)
        })
    };

    getPeoples = () => {
        fetch("https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7")
            .then(response => response.json())
            .then(responseJson => this.flattenPeoples(responseJson.people))
            .then((flattenPeople) => this.setState({peoples: flattenPeople, loading: false}));
    };

    setViewId = (viewId) => {
      this.setState({viewId})
    };

    setSearch = (search) => {
        if (search === "") {this.setState({filteredPeoples: undefined, search}); return}
        const {selectedColumn, peoples} = this.state;
        let filteredPeoples = [];
        if (selectedColumn) {
            filteredPeoples = peoples.filter((person) =>{
                return person[selectedColumn].toString().toLowerCase().includes(search.toLowerCase())}
            )
        } else {
            filteredPeoples = peoples.filter((person) =>{

                return JSON.stringify(Object.values(person)).toLowerCase().includes(search.toLowerCase())}
            );

        }
        this.setState({search, filteredPeoples})
    };

    setSelectedColumn = (selectedColumn) => {
        this.setState({selectedColumn})
    };

    setPeoples = (peoples) => {
        this.setState({peoples})
    };

    displayNavigation = (viewId, peoples, filteredPeoples, selectedColumn) => {
        switch (viewId) {
            case (0):
                return (
                    <TableView
                        setPeoples={this.setPeoples}
                        setSelectedColumn={this.setSelectedColumn}
                        onRowSelected={this.onRowSelected}
                        peoples={filteredPeoples || peoples}
                    />
                );
            case (1):
                return (
                    <PieChartView peoples={peoples} selectedColumn={selectedColumn || "gender"}  />
                );
            case (2):
                return (
                    <BarChartView peoples={peoples} selectedColumn={selectedColumn || "gender"}  />
                );
            case (3):
                return (
                    <MapView peoples={peoples} />
                );
            default:
                break;
        }
    };

    render() {
        const { navigationOpen, peoples, loading, viewId, search, filteredPeoples, selectedColumn } = this.state;
        if (loading) {return (<p> loading </p>)}
        return (
            <div className="App">
                <Header
                    search={search}
                    setSearch={this.setSearch}
                    setNavigationOpen={this.setNavigationOpen}
                    searchBy={selectedColumn}
                />
                <DrawerNavigation
                    isOpen={navigationOpen}
                    setViewId={this.setViewId}
                    setNavigationOpen={this.setNavigationOpen}
                />
                {
                   this.displayNavigation(viewId, peoples, filteredPeoples, selectedColumn)
                }
            </div>
        );
    }
}

export default App;
